<html>
  <head>
    <title>Hellfire</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel='stylesheet' href='/assets/vendor/vis/dist/vis.min.css'>
    <link rel='stylesheet' href='/assets/vendor/vis/dist/vis-timeline-graph2d.min.css'>
    <!-- <link rel="stylesheet" href="/assets/vendor/datepicker.min.css"> -->
    <link rel="stylesheet" href="/assets/vendor/datepicker.css">
    <link rel="stylesheet" href="/assets/vendor/vue-multiselect.min.css">
    <link rel='stylesheet' href='/assets/css/main.css' />
  </head>

  <body>

    <div class="container-fluid">
      <div class="col-lg-2 col-md-2 col-sm-2" style="border: 1px solid red">
        <? require $_SERVER['DOCUMENT_ROOT'] . '/inc/sidebar.php'; ?>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-10">